TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

INCLUDEPATH += .
INCLUDEPATH += /usr/local/include/GLFW


LIBS += -lglfw -lGLEW -lGL -lGLU -lSOIL -lassimp

SOURCES += main.cpp \
    GLSLShader.cpp \
    LoadScene.cpp \
    Mesh.cpp

HEADERS += \
    GLSLShader.h \
    defines.h \
    LoadScene.h \
    Mesh.h \
    MeshData.h

