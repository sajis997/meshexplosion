#ifndef MESHDATA_H
#define MESHDATA_H

#include "defines.h"

struct VertexData
{
    glm::vec3 position;//0 1 2
    glm::vec3 normal;//3 4 5
    glm::vec3 tangent;//6 7 8
    glm::vec3 color;//9 10 11
    glm::vec2 uv;//12
};

struct TextureData
{
    GLuint id;
    GLuint type;
};


#endif // MESHDATA_H
