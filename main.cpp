#include "defines.h"
#include "LoadScene.h"

int winWidth = 512;
int winHeight = 512;

GLFWwindow *window = NULL;

GLSLShader *shader =NULL;

LoadScene *scene = NULL;

GLuint vaoID;

GLfloat zoom = 2.f;

glm::mat4 model = glm::mat4(1.0f);
glm::mat4 view = glm::mat4(1.0f);
glm::mat4 projection = glm::mat4(1.0f);

void loadShaders();
void startup();
void render(float);
void shutdown();

static void error_callback(int error, const char* description);
static void framebuffer_size_callback(GLFWwindow* window, int width, int height);
static void scroll_callback(GLFWwindow* window, double x, double y);

int main()
{
    // Initialise GLFW before using any of its function
    if( !glfwInit() )
    {
       std::cerr << "Failed to initialize GLFW." << std::endl;
       exit(EXIT_FAILURE);
    }

    glfwWindowHint(GLFW_SAMPLES, 4);
    //we want the opengl 4
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 4);
    //we do not want the old opengl
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // Open a window and create its OpenGL context
    window = glfwCreateWindow( winWidth, winHeight, "Bunny Explosion", NULL, NULL);

    if( window == NULL )
    {
       fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 4.3 compatible. Try the 2.1 version of the tutorials.\n" );

       //we could not initialize the glfw window
       //so we terminate the glfw
       glfwTerminate();
       exit(EXIT_FAILURE);
    }

    glfwSetErrorCallback(error_callback);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    glfwSetScrollCallback(window, scroll_callback);


    //make the current window context current
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1);

    glfwGetFramebufferSize(window,&winWidth,&winHeight);
    framebuffer_size_callback(window,winWidth,winHeight);

    // Initialize GLEW
    glewExperimental = true; // Needed for core profile

    if (glewInit() != GLEW_OK)
    {
       fprintf(stderr, "Failed to initialize GLEW\n");
       exit(EXIT_FAILURE);
    }

    if(!glewIsSupported("GL_VERSION_4_4"))
    {
       std::cerr << "OpenGL version 4.4 is yet to be supported. " << std::endl;
       exit(1);
    }

    while(glGetError() != GL_NO_ERROR) {}

    //print out information aout the graphics driver
    std::cout << std::endl;
    std::cout << "OpenGL Version: " << glGetString(GL_VERSION) << std::endl;
    std::cout << "GLSL Version: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;
    std::cout << "GLEW Verion: " << glewGetString(GLEW_VERSION) << std::endl;
    std::cout << "OpenGL Vendor: " << glGetString(GL_VENDOR) << std::endl;
    std::cout << "Renderer: " << glGetString(GL_RENDERER) << std::endl;

    //do the initialization once and only
    startup();


    do{
       render((float)glfwGetTime());
       // Swap buffers
       glfwSwapBuffers(window);
       glfwPollEvents();

    } // Check if the ESC key was pressed or the window was closed
    while( glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS &&
       glfwWindowShouldClose(window) == 0 );


    //once the following function is called
    //no more events will be dilivered for that
    //window and its handle becomes invalid
    glfwDestroyWindow(window);

    // Close OpenGL window and terminate GLFW
    glfwTerminate();

    shutdown();

    exit(EXIT_SUCCESS);
}


void startup()
{
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    glGenVertexArrays(1,&vaoID);
    glBindVertexArray(vaoID);

    //load the shaders
    loadShaders();

    //initialize and load a scene model
    scene = new LoadScene("models/buddha.obj");

    //define the view matrix
    view = glm::lookAt(glm::vec3(0.0f,0.0f,2.0f),
                       glm::vec3(0.0f,0.0f,0.0f),
                       glm::vec3(0.0f,1.0f,0.0f));

    //get the frame buffer width and height
    glfwGetFramebufferSize(window,&winHeight,&winHeight);

    //define the projection matrix
    projection = glm::perspective(70.0f,(float)winWidth/winHeight,0.3f,100.0f);

    //get the light position in world coordinate
    glm::vec4 lightPosition = view * glm::vec4(5.0f,5.0f,2.0f,1.0f);

    shader->Use();
    //send the light position to the shader
    glUniform4f((*shader)("light.Position"),lightPosition.x,
                                            lightPosition.y,
                                            lightPosition.z,
                                            lightPosition.w);


    shader->UnUse();
}

void render(float time)
{
    time /= 7.007f;

    static const GLfloat clearColor[] = {0.0f,0.0f,0.0f,1.0f};
    static const GLfloat one = 1.0f;

    glViewport(0,0,winWidth,winHeight);
    glClearBufferfv(GL_COLOR,0,clearColor);
    glClearBufferfv(GL_DEPTH,0,&one);

    //define the model matrix
    glm::mat4 scale1 = glm::scale(glm::mat4(1.0),glm::vec3(0.8f)); // 0.05 for the elephant model
    glm::mat4 translate1 = glm::translate(glm::mat4(1.0),glm::vec3(0.0f,-0.5f,0.0f));
    glm::mat4 rotY = glm::rotate(glm::mat4(1.0f),time ,glm::vec3(0.0f,1.0f,0.0f));
    glm::mat4 translate2  = glm::translate(glm::mat4(1.0f),glm::vec3(0.0f, 0.0f, -zoom));

    model = translate2 * translate1 * scale1 * rotY;


    //setup the matrices - the model-view matrix
    glm::mat4 mv = view * model;

    shader->Use();

    glUniformMatrix4fv((*shader)("ModelviewMatrix"),1,GL_FALSE,glm::value_ptr(mv));

    //calculate the normal matrix from the modelview matrix
    glm::mat3 NormalMatrix = glm::inverseTranspose(glm::mat3(mv));

    glUniformMatrix3fv((*shader)("NormalMatrix"),1,GL_FALSE,glm::value_ptr(NormalMatrix));

    glUniformMatrix4fv((*shader)("ModelviewProjectionMatrix"),1,GL_FALSE,glm::value_ptr(projection * mv));

    glUniform1f((*shader)("explodeFactor"),sinf(time * 8.0f) * cosf(time * 6.0f) * 0.7f + 0.1f);

    scene->draw(shader);

    shader->UnUse();
}

void shutdown()
{
    if(shader)
    {
        shader->DeleteShaderProgram();
        delete shader;
        shader = NULL;
    }

    if(scene)
    {
        delete scene;
        scene = NULL;
    }

    glBindVertexArray(0);
    glDeleteVertexArrays(1,&vaoID);
}

void loadShaders()
{
    //de-allocate the shader if
    //it is already allocated
    if(shader)
    {
        shader->DeleteShaderProgram();
        delete shader;
        shader = NULL;
    }


    shader = new GLSLShader();

    shader->LoadFromFile(GL_VERTEX_SHADER,"shaders/explode.vert");
    shader->LoadFromFile(GL_GEOMETRY_SHADER,"shaders/explode.geom");
    shader->LoadFromFile(GL_FRAGMENT_SHADER,"shaders/explode.frag");

    shader->CreateAndLinkProgram();

    shader->Use();

    //add the vertex attribute
    shader->AddAttribute("vertex");
    shader->AddAttribute("normal");
    shader->AddAttribute("tangent");
    shader->AddAttribute("color");
    shader->AddAttribute("uv");

    //add the uniform to the shader
    shader->AddUniform("material.Kd");
    shader->AddUniform("light.Ld");
    shader->AddUniform("light.Position");
    shader->AddUniform("material.Ka");
    shader->AddUniform("light.La");
    shader->AddUniform("material.Ks");
    shader->AddUniform("light.Ls");
    shader->AddUniform("material.Shininess");

    shader->AddUniform("explodeFactor");


    //since the above uniform values will remain constant
    //for the lifetime of the application, we can set
    //the values during initialization
    glUniform3f((*shader)("material.Kd"),0.9f,0.5f,0.3f);
    glUniform3f((*shader)("light.Ld"),1.0f,1.0f,1.0f);
    glUniform3f((*shader)("material.Ka"),0.9f,0.5f,0.3f);
    glUniform3f((*shader)("light.La"),0.4f,0.4f,0.4f);
    glUniform3f((*shader)("material.Ks"),0.8f,0.8f,0.8f);
    glUniform3f((*shader)("light.Ls"),1.0f,1.0f,1.0f);
    glUniform1f((*shader)("material.Shininess"),100.0f);

    shader->AddUniform("ModelviewMatrix");
    shader->AddUniform("NormalMatrix");
    shader->AddUniform("ProjectionMatrix");
    shader->AddUniform("ModelviewProjectionMatrix");

    shader->UnUse();
}

void error_callback(int error, const char* description)
{

}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    if(height < 1)
        height = 1;

    winWidth = width;
    winHeight = height;

    glViewport(0,0,winWidth,winHeight);

    projection = glm::perspective(70.0f,(float)winWidth/winHeight,0.3f,100.0f);
}


void scroll_callback(GLFWwindow* window, double x, double y)
{
   //increase or decrease field of view based on mouse wheel
   zoom += (float) y / 4.f;
   if (zoom < 0)
      zoom = 0;
}
