#include "Mesh.h"


Mesh::Mesh(std::vector<VertexData> *vd,
     std::vector<unsigned int> *id,
     std::vector<TextureData> *td)
{
    //initialize the member variable
    data = *vd;
    indices = *id;

    if(td)
        textures = *td;

    glGenBuffers(1,&vboID);
    //create buffer to hold the vertex data from the mesh
    glBindBuffer(GL_ARRAY_BUFFER,vboID);
    glBufferData(GL_ARRAY_BUFFER,data.size() * sizeof(VertexData),&data[0],GL_STATIC_DRAW);


    glGenBuffers(1,&vboElementID);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vboElementID);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,indices.size() * sizeof(unsigned int),&indices[0],GL_STATIC_DRAW);


    glBindBuffer(GL_ARRAY_BUFFER,0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);
}

Mesh::~Mesh()
{
    //clear the STL containers
    data.clear();
    indices.clear();
    textures.clear();

    glDeleteBuffers(1,&vboID);
    glDeleteBuffers(1,&vboElementID);


}

void Mesh::draw(GLSLShader *shader)
{
    //get the vertex attribute location for the shader
    //and those attributes will be bound to the shader
    GLuint vertexLoc = (*shader)["vertex"];
    GLuint normalLoc = (*shader)["normal"];
    GLuint tangentLoc = (*shader)["tangent"];
    GLuint colorLoc = (*shader)["color"];
    GLuint textureLoc = (*shader)["uv"];

    std::string str = "texture";
    std::stringstream convert;
    std::string result;

    for(int i = 0;i < textures.size();i++)
    {
        glActiveTexture(GL_TEXTURE0 + i);
        glBindTexture(GL_TEXTURE_2D,textures[i].id);

        convert << i;
        result = str + convert.str() + "0";

        glUniform1i((*shader)(result),i);
    }

    glBindBuffer(GL_ARRAY_BUFFER,vboID);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vboElementID);

    glEnableVertexAttribArray(vertexLoc);
    glVertexAttribPointer(vertexLoc,3,GL_FLOAT,GL_FALSE,sizeof(VertexData),BUFFER_OFFSET(0));

    glEnableVertexAttribArray(normalLoc);
    glVertexAttribPointer(normalLoc,3,GL_FLOAT,GL_FALSE,sizeof(VertexData),BUFFER_OFFSET(3*sizeof(float)));


    glEnableVertexAttribArray(tangentLoc);
    glVertexAttribPointer(tangentLoc,3,GL_FLOAT,GL_FALSE,sizeof(VertexData),BUFFER_OFFSET(6*sizeof(float)));

    glEnableVertexAttribArray(colorLoc);
    glVertexAttribPointer(colorLoc,3,GL_FLOAT,GL_FALSE,sizeof(VertexData),BUFFER_OFFSET(9*sizeof(float)));

    glEnableVertexAttribArray(textureLoc);
    glVertexAttribPointer(textureLoc,2,GL_FLOAT,GL_FALSE,sizeof(VertexData),BUFFER_OFFSET(12*sizeof(float)));

    glDrawElements(GL_TRIANGLES,indices.size(),GL_UNSIGNED_INT,BUFFER_OFFSET(0));


    glDisableVertexAttribArray(vertexLoc);
    glDisableVertexAttribArray(normalLoc);
    glDisableVertexAttribArray(tangentLoc);
    glDisableVertexAttribArray(colorLoc);
    glDisableVertexAttribArray(textureLoc);

    glBindBuffer(GL_ARRAY_BUFFER,0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);
}
