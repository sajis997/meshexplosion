#ifndef MESH_H
#define MESH_H

#include "MeshData.h"
#include "GLSLShader.h"

class Mesh
{
private:
    std::vector<VertexData> data;
    std::vector<TextureData> textures;
    std::vector<unsigned int> indices;

    GLuint vboID;
    GLuint vboElementID;

public:
    Mesh(std::vector<VertexData> *vd,
         std::vector<unsigned int> *id,
         std::vector<TextureData> *td = NULL);
    ~Mesh();

    void draw(GLSLShader*);

};

#endif // MESH_H
