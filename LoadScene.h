#ifndef LOADSCENE_H
#define LOADSCENE_H

#include "Mesh.h"

class LoadScene
{
private:
    std::vector<Mesh*> meshes;
    void recursiveProcess(aiNode *node,const aiScene *scene);
    void processMesh(aiMesh *mesh,const aiScene *scene);
    unsigned int loadTexture(const char* fileName);

    //hold the texture id
    GLuint textureID;
public:
    LoadScene(const char* filename);
    ~LoadScene();
    void draw(GLSLShader *shader);
    std::vector<Mesh*>& getMeshes();
};

#endif // LOADSCENE_H
