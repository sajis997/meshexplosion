#include "LoadScene.h"
#include "GLSLShader.h"

void LoadScene::recursiveProcess(aiNode *node,const aiScene *scene)
{
    //process
    for(int i = 0; i < node->mNumMeshes;i++)
    {
        //get the mesh from the scene
        aiMesh *mesh = scene->mMeshes[node->mMeshes[i]];
        processMesh(mesh,scene);
    }


    //recursion
    for(int i = 0; i < node->mNumChildren;i++)
    {
        recursiveProcess(node->mChildren[i],scene);
    }
}

void LoadScene::processMesh(aiMesh *mesh,const aiScene *scene)
{
    //declare STL vector to populate them with
    //the mesh data
    std::vector<VertexData> data;
    std::vector<unsigned int> indices;
    std::vector<TextureData> textures;

    aiColor4D color;

    aiMaterial *mat = scene->mMaterials[mesh->mMaterialIndex];
    //get the diffuse material color
    aiGetMaterialColor(mat,AI_MATKEY_COLOR_DIFFUSE,&color);

    //and then set it as the default material color
    glm::vec3 defaultColor(color.r,color.g,color.b);

    for(unsigned int i = 0; i < mesh->mNumVertices;i++)
    {
        VertexData tmpData;
        glm::vec3 tmpVec;

        //position
        tmpVec.x = mesh->mVertices[i].x;
        tmpVec.y = mesh->mVertices[i].y;
        tmpVec.z = mesh->mVertices[i].z;

        //update the position of the vertex data
        tmpData.position = tmpVec;

        //normal
        tmpVec.x  = mesh->mNormals[i].x;
        tmpVec.y  = mesh->mNormals[i].y;
        tmpVec.z  = mesh->mNormals[i].z;

        tmpData.normal = tmpVec;


        //tangents
        //check first if tangent is there
        if(mesh->mTangents)
        {
            tmpVec.x = mesh->mTangents[i].x;
            tmpVec.y = mesh->mTangents[i].y;
            tmpVec.z = mesh->mTangents[i].z;
        }
        else
        {
            tmpVec.x = 1.0f;
            tmpVec.y = 0.0f;
            tmpVec.z = 0.0f;
        }

        tmpData.tangent = tmpVec;

        //colors
        if(mesh->mColors[0])
        {
            tmpVec.x = mesh->mColors[0][i].r;
            tmpVec.y = mesh->mColors[0][i].g;
            tmpVec.z = mesh->mColors[0][i].b;
        }
        else
        {
            //if no color is found , just make the object greyish
            tmpVec = defaultColor;
        }

        tmpData.color = tmpVec;


        //texture coordinate
        if(mesh->mTextureCoords[0])
        {
            tmpVec.x = mesh->mTextureCoords[0][i].x;
            tmpVec.y = mesh->mTextureCoords[0][i].y;
        }
        else
        {
            //if no color is found , just make the object greyish
            tmpVec.x = tmpVec.y = 0.0;
        }

        tmpData.uv = glm::vec2(tmpVec.x,tmpVec.y);

        //inset the vertex data into the vector
        data.push_back(tmpData);
    }

    //get the indices
    for(unsigned int i = 0; i < mesh->mNumFaces;i++)
    {
        aiFace face = mesh->mFaces[i];

        for(unsigned int j = 0; j < face.mNumIndices; j++) //0..2
        {
            indices.push_back(face.mIndices[j]);
        }
    }

    for(unsigned int i = 0; i < mat->GetTextureCount(aiTextureType_DIFFUSE);i++)
    {
        aiString str;
        mat->GetTexture(aiTextureType_DIFFUSE,i,&str);

        TextureData tmp;

        tmp.id = loadTexture(str.C_Str());
        tmp.type = 0; // diffuse map

        textures.push_back(tmp);

    }

    meshes.push_back(new Mesh(&data,&indices,&textures));
}

unsigned int LoadScene::loadTexture(const char* fileName)
{
    //load the image using SOIL image library
    int texture_width = 0, texture_height = 0, channels = 0;

    GLubyte* pData = SOIL_load_image(fileName,
                     &texture_width,
                     &texture_height,
                     &channels,
                     SOIL_LOAD_AUTO);

    if(pData == NULL)
    {
       cerr<<"Cannot load image: "<< fileName <<endl;
       exit(EXIT_FAILURE);
    }

    //setup opengl texture

    //generate a name for the texture
    glGenTextures(1, &textureID);
    glBindTexture(GL_TEXTURE_2D,textureID);

    //set texture parameters
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);


    //allocate texture memory
    glTexStorage2D(GL_TEXTURE_2D,1,GL_RGB8,texture_width,texture_height);

    //specify some data for the texture
    glTexSubImage2D(GL_TEXTURE_2D,0,0,0,texture_width,texture_height,GL_RGB,GL_UNSIGNED_BYTE,pData);

    //free SOIL image data
    SOIL_free_image_data(pData);

    return textureID;

}


LoadScene::LoadScene(const char* filename)
{
    Assimp::Importer importer;
    const aiScene *scene = importer.ReadFile(filename,aiProcess_GenSmoothNormals |
                                                      aiProcess_Triangulate |
                                                      aiProcess_FlipUVs |
                                                      aiProcess_CalcTangentSpace |
                                                      aiProcess_ImproveCacheLocality |
                                                      aiProcess_OptimizeGraph |
                                                      aiProcess_OptimizeMeshes |
                                                      aiProcess_JoinIdenticalVertices |
                                                      aiProcess_RemoveRedundantMaterials);


    if(!(scene->mRootNode) || (scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE))
    {
        std::cout << "The file was not opened successfully :" << filename << std::endl;
        exit(EXIT_FAILURE);
    }

    recursiveProcess(scene->mRootNode,scene);
}

LoadScene::~LoadScene()
{
    for(unsigned int i = 0 ; i < meshes.size();i++)
    {
        delete meshes[i];
        meshes[i] = NULL;
    }


    glDeleteTextures(1,&textureID);
}

void LoadScene::draw(GLSLShader *shader)
{
    for(unsigned int i = 0; i < meshes.size();i++)
        meshes[i]->draw(shader);
}

std::vector<Mesh*>& LoadScene::getMeshes()
{
    return meshes;
}
