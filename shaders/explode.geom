#version 430 core

in VS_OUT
{
	vec3 vertexPosition;
	vec3 vertexNormal;
} gs_in[];

layout (triangles) in;
layout (triangle_strip,max_vertices = 3) out;

out GS_OUT
{
  vec3 gNormal;
  vec3 gPosition;
} gs_out;

uniform float explodeFactor = 0.2;

void main()
{
   vec3 ab = gl_in[1].gl_Position.xyz - gl_in[0].gl_Position.xyz;
   vec3 ac = gl_in[2].gl_Position.xyz - gl_in[0].gl_Position.xyz;

   vec3 faceNormal = normalize(cross(ac,ab));

   for(int i = 0; i < gl_in.length(); i++)
   {
	gl_Position = gl_in[i].gl_Position + vec4(faceNormal * explodeFactor,1.0);

	//the rest is just a pass-through
	gs_out.gNormal = gs_in[i].vertexNormal;
	gs_out.gPosition = gs_in[i].vertexPosition;

	EmitVertex();
   }

   EndPrimitive();
}