#version 430 core

//the following vertex attributes will 
//go from the mesh we have defined 
//in our application
in vec3 vertex;
in vec3 normal;
in vec3 tangent;
in vec3 color;
in vec2 uv;

out vec3 vertexPosition;
out vec3 vertexNormal;

uniform mat4 ModelviewMatrix;
uniform mat3 NormalMatrix;
uniform mat4 ProjectionMatrix;
uniform mat4 ModelviewProjectionMatrix;

void main()
{
    vertexNormal = normalize(NormalMatrix * normal);

    //vertex position in the eye coordinates
    vertexPosition = vec3(ModelviewMatrix * vec4(vertex,1.0));

    gl_Position = ModelviewProjectionMatrix * vec4(vertex,1.0);
}