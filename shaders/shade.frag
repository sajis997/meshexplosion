#version 430 core


in vec3 vertexPosition;
in vec3 vertexNormal;


//have the light information
//encapsulated in a struct
struct LightInfo
{
   vec4 Position; // light position in the eye coordinates
   vec3 La;       // ambient light intensity
   vec3 Ld;       // diffuse light intensity
   vec3 Ls;       // specular light intensity
};

uniform LightInfo light;


//have the material information
//encapsulated in a struct
struct MaterialInfo
{
   vec3 Ka;          // ambient reflectivity
   vec3 Kd;          // diffuse reflectivity
   vec3 Ks;          // specular reflectivity
   float Shininess;  // specular shininess factor	
};

uniform MaterialInfo material;

out vec4 fragColor;

void main()
{
   vec3 lightIntensity;	

   vec3 tnorm = normalize(vertexNormal);

   vec3 s = normalize(vec3(light.Position - vec4(vertexPosition,1.0)));

   vec3 v = normalize(-vertexPosition);

   //calculate the half-way vector
   vec3 h = normalize(s + v);

   //get the reflection vector
   //vec3 r = reflect(-s, tnorm);

   vec3 ambient = light.La * material.Ka;

   float sDotN = max(dot(s,tnorm),0.0);

   vec3 diffuse = light.Ld * material.Kd * sDotN;

   vec3 spec = vec3(0.0);

   if(sDotN > 0.0)
   	  spec = light.Ls * material.Ks * pow(max(dot(h,tnorm),0.0),material.Shininess);

   lightIntensity = ambient + diffuse + spec;

   fragColor = vec4(lightIntensity,1.0);
}
