#version 430 core

in vec3 vertex;
in vec3 normal;
in vec3 tangent;
in vec3 color;
in vec2 uv;



out VS_OUT
{
	vec3 vertexPosition;
	vec3 vertexNormal;

} vs_out;

uniform mat4 ModelviewMatrix;
uniform mat3 NormalMatrix;
uniform mat4 ProjectionMatrix;
uniform mat4 ModelviewProjectionMatrix;

void main()
{
    vs_out.vertexNormal = normalize(NormalMatrix * normal);

    //vertex position in the eye coordinates
    vs_out.vertexPosition = vec3(ModelviewMatrix * vec4(vertex,1.0));

    gl_Position = ModelviewProjectionMatrix * vec4(vertex,1.0);
}
